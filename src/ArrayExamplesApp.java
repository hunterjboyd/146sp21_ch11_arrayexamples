
import java.util.Arrays;

/**
 * Application for testing out various ways of working with arrays
 * (based on material from Murach's Java Programming 5/e, Ch. 11)
 * @author hjboyd@email.sc.edu
 * @version 1.0
 */
public class ArrayExamplesApp {
    
    /**
     * Main method for the app
     * @param args
     */
    public static void main( String[] args ) 
    {
        // Ch11 Slide 10
        int[] values = new int[10];
        for (int i = 0; i < values.length; i++) { 
           values[i] = i;
           System.out.println( "i = " + i + ", " + "values[i] = " + values[i] );
        } // end for

        // Instructions to students:
        // After we go through a given example, we'll move the starting
        // comment delimiter, /*, downward in the source code in order to 
        // execute the next example...
        
        
        System.out.println("-------------------------------");
        
        // Ch11 Slide 11
        // the array `prices` is declared, instantitiated, and initialized
        // all in a single statement. The initialization is accomplished
        // by means of an ARRAY INITIALIZER (comma-separated list of values
        // coded inside curly braces)
        double[] prices = {14.95, 12.95, 11.95, 9.95};
        for (int i = 0; i < prices.length; i++) { 
           System.out.println(prices[i]);
        } // end for
        
        
        System.out.println("-------------------------------");
        
        // Ch11 Slide 12
        double sum = 0.0;  // `sum` is our accumulator variable
        System.out.println("Starting value of sum = " + sum);
        for (int i = 0; i < prices.length; i++) { 
            sum += prices[i];
            System.out.println("For iteration " + i + 
                    ", current value of sum = " + sum );
        } // end for
        System.out.println("Final value of sum = " + sum );
        double average = sum / prices.length;
        System.out.println("Average price = " + average );
        
        
        System.out.println("-------------------------------");
        
        // Ch11 Slide 13 (note: uses same prices array as the
        // the counter-controlled for loop in slide 11 above)
        System.out.println("Here, we use an enhanced for loop to "
            + "DISPLAY the elements of the prices array...");
        for ( double currentPrice : prices )
        {
            System.out.println( currentPrice );
        } // end enhanced for loop
        
        System.out.println(
            "Note what happens when you try to use an enhanced for loop\n"
            + "to MODIFY the elements of the array...");
        
        for ( double currentPrice : prices )
        {
            // currentPrice does NOT "point" to the original array element;
            // it is simply a local variable that contains a COPY of 
            // the original array element that is of a primitive type (double)
            currentPrice = 2 * currentPrice; 
            System.out.println( currentPrice ); 
        } // end enhanced for loop
        
        System.out.println("But the original prices are NOT modified:");
        for ( double currentPrice : prices )
        {
            System.out.println( currentPrice );
        } // end enhanced for loop
        
        System.out.println(
            "To modify the primitive-type array elements, you'd have \n"
            + "to use a counter-controlled loop to DIRECTLY access them:");
        for (int i = 0; i < prices.length; i++) { 
            prices[i] = 2 * prices[i];
        } // end for
        
        // Now, we'll see that the original prices are modified:
        for ( double currentPrice : prices )
        {
            System.out.println( currentPrice );
        } // end enhanced for loop
        
        
        
        System.out.println("-------------------------------");
        
        // here's variation on slide 13, but uses reference-type array elements
        // that happen to be MUTABLE (i.e., they can be changed)
        Product[] products = { new Product("java", "java book", 9.99),
                               new Product("cpp", "c++ book", 11.99),
                               new Product("python", "python book", 13.99) };
        
        /*
        System.out.println("Here are the original prices of the books:");
        for ( Product currentProduct : products )
        {
            System.out.println( currentProduct.getPriceFormatted() );
        } // end enhanced for loop
        
        System.out.println(
            "Here, we will use an enhanced for loop to set each book's price...");
        for ( Product currentProduct : products )
        {
            // currentProduct is a REFERENCE (or "pointer") to the 
            // same object that the original array element refers to!!
            currentProduct.setPrice(1.00);
        } // end enhanced for loop
        
        System.out.println("Let's see if the prices are really updated:");
        for ( Product currentProduct : products )
        {
            System.out.println( currentProduct.getPriceFormatted() );
        } // end enhanced for loop
        */
        
        System.out.println("-------------------------------");
        
        // Ch11 Slide 14 
        // Use an enhanced for loop to compute the average price
        // (Enter this code yourself)
        sum = 0.0;
        for ( Product currentProduct : products ){
            sum += currentProduct.getPrice();
            
        }
        average = sum / products.length;
        System.out.println("Average = " + average);
        
        System.out.println("-------------------------------");
        
        // Ch11 Slide 16 & 17
        // How to fill in an array using Arrays.fill
        // (Note the need to import java.util.Arrays above!)
        int[] quantities = new int[5];   // all elements set to 0
        Arrays.fill(quantities, 1);      // all elements set to 1
        
        // Not in your book:
        // Use the STATIC UTILITY METHOD toString (of the Arrays class) 
        // to get a reasonably useful string representation of a 1-D array
        System.out.println( Arrays.toString( quantities ) );
        
        // How to sort an array using Arrays.sort
        int[] numbers = {2,6,4,1,8,5,9,3,7,0};
        Arrays.sort(numbers); // uses a variation on the "quicksort" algorithm
                              // this method mutates the original array, it
                              // does not create a copy
        for (int number : numbers) {
            System.out.print(number + " " );
        } // end enhanced for
        System.out.println();
        
        // How to search an array using Arrays.binarySearch
        // note that binary search only works in the array is sorted beforehand
        String[] productCodes = {"mysql", "jsp", "java"};
        System.out.println( "Before sort: " + Arrays.toString( productCodes ) );
        Arrays.sort(productCodes);  // sorts in place
        System.out.println( "After sort: " + Arrays.toString( productCodes ) );
        int index = Arrays.binarySearch(productCodes, "mysql"); // sets index to 2
        System.out.println("mysql is at index " + index);
        
        
        System.out.println("-------------------------------");
        
        // Ch11 Slide 19
        // How to create a reference to an array
        // (Note that we are using different array names than the book uses)
        double[] grades = {92.3, 88.0, 95.2, 90.5};
        double[] anotherReferenceToGrades = grades;
        anotherReferenceToGrades[1] = 70.2;       // changes grades[1] too
        System.out.println("grades[1]=" + grades[1]);  // 70.2

        // How to create a "shallow copy" of an array
        // First, we'll "reset" grades[1] back to its original value
        // (for the purposes of this demo)
        grades[1] = 88.0;
        // now we'll create a "shallow copy" of grades 
        // NOTE that this is NOT another REFERENCE to grades 
        // -- it's another array entirely, but just happens to
        //    contain identical data!!
        double[] gradesCopy = Arrays.copyOf(
            grades, grades.length);
        gradesCopy[1] = 70.2;       // doesn't change grades[1]
        System.out.println("grades[1]=" + grades[1]);  // 88.0

        // How to determine if two variables refer to ("point to")
        // the same array object
        // (Try changing anotherReferenceToGrades to gradesCopy and
        //  see what happens!)
        if (grades == gradesCopy) {
            System.out.println(
                "Both variables refer to the same array.");
        } else {
            System.out.println(
                "Each variable refers to a different array.");
            System.out.println(
                "But these arrays may contain the same data.");
        }
        
        // How to determine if variables refer to arrays that
        // may or may not be the same array, but they do contain
        // identical data elements
        if (Arrays.equals(grades, anotherReferenceToGrades)) {
            System.out.println(
                "Both variables contain the identical.");
        } else {
            System.out.println(
                "Both variables do not contain identical data.");
        }
        
        System.out.println("-------------------------------");
        
        // Ch11 Slides 24 & 25 (ignore this)
        // NOTE: this uses the same 1-D array of Products as listed above
        products[0].setPrice(9.99);
        products[1].setPrice(14.99);
        products[2].setPrice(6.99);
                        
        Arrays.sort(products); // sorting is determined based on 
                               // how you implement the compareTo method
                               // in the Product class
                               
        for (Product p : products) {
            System.out.println(p.getPrice() + ": " + p.getDescription());
        }
        
        /* <-- move this down as needed
        */
        
    } // end method main
    
} // end class ArrayExamplesApp
